/*
 * Copyright 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Test client for use with social application (as an OAuth2 auth server). Remember to
 * access this app via its IP address (not "localhost"), otherwise the auth server will
 * steal your cookie.
 * 
 * @author Dave Syer
 *
 */
/***

@EnableAutoConfiguration
//此框架的神奇之处在于@EnableAutoConfiguration注释，此注释自动载入应用程序所需的所有Bean——这依赖于Spring Boot在类路径中的查找。
创建系统的bean;

* 说明;
 *
 * 这里相当于中间件了，访问这个中间件
 *
 */
@EnableAutoConfiguration  //此框架的神奇之处在于@EnableAutoConfiguration注释，此注释自动载入应用程序所需的所有Bean——这依赖于Spring Boot在类路径中的查找。
@Configuration //当做配置文件类初始包和EnableAutoConfiguration 结合使用
@EnableOAuth2Sso //这是单点登录的标记
@RestController
//@SpringBootApplication
/**1 访问9999/cleint
 * 2 转向了9999/login 系统生成的
 * 3 重定向到/8080//oauth/authorize去取access_code，需要登录
 * 4 登录成功之后，/login?code 带有code回来，再session中记录用户的sessionId
 * 5 正常登录成功
 */
public class ClientApplication {

	@RequestMapping("/client")
	public String home(Principal user) {
		return "Hello " + user.getName();
	}

	public static void main(String[] args) {
		new SpringApplicationBuilder(ClientApplication.class).run(args);
	}

}
